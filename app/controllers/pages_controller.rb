class PagesController < ApplicationController
  before_filter :authenticate_user!, :only => [:add_map, 
                                                :add_marker, 
                                                :update_map, 
                                                :fetch_map_details, 
                                                :del_map
                                               ]

  def landing
     if user_signed_in?
      redirect_to action: "home"
    end
  end

  def home
  	if !user_signed_in?
      redirect_to action: "landing"
    end
    @maps = Map.where(:user_id => current_user.id).all

  end

  
  # AJAX call to add new map entry
  def add_map
    if request.xhr?
      map_name = params[:map_name]
      upper_right_lat = params[:upper_right_lat]
      upper_right_long = params[:upper_right_long]
      lower_left_lat = params[:lower_left_lat]
      lower_left_long = params[:lower_left_long]
      user_id = current_user.id
      map = Map.new(:map_name => map_name,
                    :user_id => user_id,
                    :urlat => upper_right_lat,
                    :urlong => upper_right_long,
                    :lllat => lower_left_lat,
                    :llong => lower_left_long)
      map.save
      render :json => {:message => "Map added successfully",:map_id => map.id}, :status => 200 and return  
    else
      render :json => {:error => "Request Failed."}, :status => 400 and return
    end
  end 

   # AJAX call to create marker
  def add_marker
    if request.xhr?
    
      map_id = params[:map_id]
      marker_name = params[:marker_name]
      marker_descripton = params[:description]
      numer_of_residents = params[:numer_of_residents]
      lat = params[:latitude]
      long = params[:longitude]
      
      marker = Marker.new(:map_id => map_id,
                    :name => marker_name,
                    :description => marker_descripton,
                    :residents_count => numer_of_residents,
                    :lat => lat,
                    :long => long)
      marker.save

      render :json => {:message => "Marker added successfully",:marker_id => marker.id}, :status => 200 and return  
    else
      render :json => {:error => "Request Failed."}, :status => 400 and return
    end
  end 


  # AJAX call to create map
  def update_map
    if request.xhr?
      map_id = params[:id]
      map_name = params[:map_name]
      upper_right_lat = params[:upper_right_lat]
      upper_right_long = params[:upper_right_long]
      lower_left_lat = params[:lower_left_lat]
      lower_left_long = params[:lower_left_long]
      user_id = current_user.id
      map = Map.find(map_id)
      if !map.nil?
        map.map_name = map_name
        map.urlat = upper_right_lat
        map.urlong = upper_right_long
        map.lllat = lower_left_lat
        map.llong = lower_left_long
        if map.save
           render :json => {:message => "Map updated successfully",:map_id => map.id}, :status => 200 and return  
        end
      else
        render :json => {:error => "Request Failed."}, :status => 400 and return
      end 
    else
      render :json => {:error => "Request Failed."}, :status => 400 and return
    end
  end 



 def fetch_map_details
    if request.xhr?
      map_id = params[:map_id]
      map = Map.find(map_id)
      if(!map.nil?)
         map_name = map.map_name
         upper_right_lat = map.urlat
         upper_right_long = map.urlong
         lower_left_lat = map.lllat
         lower_left_long = map.llong
         markers = Marker.where(:map_id => map_id) 
         render :json => {:map_name => map_name,:upper_right_lat => upper_right_lat,:upper_right_long => upper_right_long,:lower_left_lat => lower_left_lat,:lower_left_long => lower_left_long,:markers => markers}, :status => 200 and return  
      else
        render :json => {:message => "Request Failed",:map_id => map.id}, :status => 400 and return
      end   
    else
      render :json => {:error => "Request Failed."}, :status => 400 and return
    end
 end    

 
def del_map
    if request.xhr?
      map_id = params[:map_id]
      map = Map.find(map_id)
      if(!map.nil?)
         map.destroy
         render :json => {:message => "Success"}, :status => 200 and return  
      else
        render :json => {:message => "Request Failed"}, :status => 400 and return
      end   
    else
      render :json => {:error => "Request Failed."}, :status => 400 and return
    end
 end  

end
