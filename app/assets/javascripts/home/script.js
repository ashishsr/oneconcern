$( document ).ready(function() {
    $('#mySavedMapModal').on('shown.bs.modal', function () { // chooseLocation is the id of the modal.
    });
});

var map;
function deleteMap(map_id){
	  $.ajax({
            url: '/pages/del_map',
            type: 'POST',
            dataType: 'json',
            data: {
                "map_id": map_id
            }
        })
        .done(function(response) {

               $("#mapRow" + map_id).remove();           
         });
}


function editMap(map_id){
   // Setting the map id in the hidden field
   $( "#editMapId" ).val(map_id);
   $("#btnUpdateMap").removeClass('hidden');
   $("#btnAddMap").addClass('hidden');

   // Fecth all the details 
   $.ajax({
            url: '/pages/fetch_map_details',
            type: 'GET',
            dataType: 'json',
            data: {
                "map_id": map_id
            }
        })
        .done(function(response) {
           setFormFields(response.map_name,response.upper_right_lat,response.upper_right_long,response.lower_left_lat,response.lower_left_long);            
           $('#addMapModal').modal('show');  
     });
}


function updateMap(map_id){
  // Fechting all the form values
 var id = $( "#editMapId" ).val();
 var name = $( "#idMapNameText" ).val();
 var upperRightLatitude = $( "#idUpperRightLat" ).val();
 var upperRightLongitude = $( "#idUpperRightLongitute" ).val();
 var lowerLeftLatitude = $( "#idLowerLeftLatitude" ).val();
 var lowerLeftLongitude = $( "#idLowerLeftLongitude" ).val();
 if(!name || !upperRightLatitude || !lowerLeftLatitude || !lowerLeftLongitude){
   $("#idMapFormMessage").removeClass("hidden");
   return;
  }

 updateMapHelper(id,name,upperRightLatitude,upperRightLongitude,lowerLeftLatitude,lowerLeftLongitude);

}

 
 function updateMapHelper(id,name,upperRightLatitude,upperRightLongitude,lowerLeftLatitude,lowerLeftLongitude){
	$.ajax({
            url: '/pages/update_map',
            type: 'POST',
            dataType: 'json',
            data: {
            	"id":id,
                "map_name": name,
                "upper_right_lat": upperRightLatitude,
                "upper_right_long": upperRightLongitude,
                "lower_left_lat" : lowerLeftLatitude,
                "lower_left_long" : lowerLeftLongitude
            }
        })
        .done(function(response) {
               //alert(response.message);
               var map_id = id;
                $("#mapRow" + map_id).remove(); 
               var htmlToBeAdded = "<div class=\"row\" id=\"mapRow"+map_id+"\">\
                <div class=\"col-sm-6 pull-left\">"+name+"</div>\<div class=\"col-sm-2\">\<button type=\"button\" class=\"btn btn-success\" onClick=\"loadMap("+map_id+");\">View Map</button>\
                </div>\
                <div class=\"col-sm-2\">\
                	<button type=\"button\" class=\"btn btn-primary\" onClick=\"editMap("+map_id+");\">Edit</button>\
                </div>\
                <div class=\"col-sm-2 pull-right\">\
                	<button type=\"button\" class=\"btn btn-danger\" onClick=\"deleteMap("+map_id+");\">Delete</button>\
                </div>\
                </div>"; 
                $( "#idMapList" ).append(htmlToBeAdded);
                setFormFields('','','','','');
                $('#addMapModal').modal('toggle'); 
            })
        .fail(function(XMLHttpRequest, textStatus, errorThrown) {
            console.error("Error Occured!" + " | " + XMLHttpRequest.responseText +
                            " | " + textStatus + " | " + errorThrown);
        })
        .always(function() {});
}




function loadAddModal(){
	$( "#editMapId" ).val('');
   $("#btnUpdateMap").addClass('hidden');
   $("#btnAddMap").removeClass('hidden');
   setFormFields('','','','','');
   $('#addMapModal').modal('show');  
}



function loadMap(map_id){
	  // Fetch map bound details from the db
	  $.ajax({
            url: '/pages/fetch_map_details',
            type: 'GET',
            dataType: 'json',
            data: {
                "map_id": map_id
            }
        })
        .done(function(response) {
               //alert(response.upper_right_lat);
               markerArray = JSON.parse(JSON.stringify(response.markers));
               prepareMap(map_id,response.upper_right_lat,response.upper_right_long,response.lower_left_lat,response.lower_left_long,markerArray);
               $('#mySavedMapModal').modal('show');           
         });

}

function prepareMap(map_id,rightLat,rightLong,leftLat,leftLong,markerArray){
	mapboxgl.accessToken = 'pk.eyJ1IjoiYXNoaXNob2MiLCJhIjoiY2ozc2ZoZ2dvMDAwMDJ3b2c1aWczMW1yNSJ9.4giPTDQOVRFzFphh41HVUA';
    
      var bounds = [
       [leftLong,leftLat], // Southwest coordinates
       [rightLong, rightLat]  // Northeast coordinates
      ];
      map = new mapboxgl.Map({
      container: 'map', // container id
      style: 'mapbox://styles/mapbox/streets-v9', //stylesheet location
      zoom: 9, // starting zoom
      maxBounds: bounds
      });

      map.on('load', function () {
      	    if(markerArray.length > 0){
      	    	for(var i=0;i<markerArray.length;i++){
      	    		addMarkerToMap(markerArray[i].id,markerArray[i].name,markerArray[i].description,markerArray[i].residents_count,markerArray[i].lat,markerArray[i].long);
      	    	}
      	    }
      	   // addMarkerToMap(1,'Mapbox DC','monument','Tesdsdsdsdsdsds',37.99867930310222,-87.60298957824573);		   
      });


     map.on('click', function (e) {
        var json = JSON.parse(JSON.stringify(e.lngLat));
        var latitude =  json.lat;
        var longitude = json.lng;
        
        clearMarkerFields();
        $( "#idMarkerLatitude" ).val(latitude);
        $( "#idMarkerLongitude" ).val(longitude);
        $( "#markerMapId" ).val(map_id);
        $("#idMarkerFormMessage").removeClass("hidden");
        $("#idMarkerFormMessage").addClass("hidden");
        $('#addMarkerModal').modal('show'); 
      });

}

function addMarker(){
   // Fething all the details
   var map_id = $( "#markerMapId" ).val();
   var latitude = $( "#idMarkerLatitude" ).val();
   var longitude = $( "#idMarkerLongitude" ).val();
   var markerName = $( "#idMarkerName" ).val();
   var description =  $( "#idMarkerDescription" ).val();
   var numberResidents = $( "#idMapNumberResidents" ).val();
   if(!markerName || !description || !numberResidents){
	   $("#idMarkerFormMessage").removeClass("hidden");
	   return;
   }
   
   addMarkerHelper(map_id,markerName,description,numberResidents,latitude,longitude);

}

function clearMarkerFields(){
	$( "#markerMapId" ).val('');
    $( "#idMarkerLatitude" ).val('');
    $( "#idMarkerLongitude" ).val('');
    $( "#idMarkerName" ).val('');
    $( "#idMarkerDescription" ).val('');
    $( "#idMapNumberResidents" ).val('');
}


function addMarkerHelper(map_id,markerName,description,numberResidents,latitude,longitude){
   $.ajax({
            url: '/pages/add_marker',
            type: 'POST',
            dataType: 'json',
            data: {
                "map_id": map_id,
                "marker_name": markerName,
                "description": description,
                "numer_of_residents" : numberResidents,
                "latitude" : latitude,
                "longitude" :   longitude
            }
        })
        .done(function(response) {
               //alert(response.message);
               addMarkerToMap(response.marker_id,markerName,description,numberResidents,latitude,longitude);


            })
        .fail(function(XMLHttpRequest, textStatus, errorThrown) {
            console.error("Error Occured!" + " | " + XMLHttpRequest.responseText +
                            " | " + textStatus + " | " + errorThrown);
        })
        .always(function() {});
}


function addMarkerToMap(marker_id,markerName,description,numer_of_residents,latitude,longitude){
   map.addLayer({
		        "id": "points"+marker_id,
		        "type": "circle",
		        "source": {
		            "type": "geojson",
		            "data": {
		                "type": "FeatureCollection",
		                "features": [{
		                    "type": "Feature",
		                    "geometry": {
		                        "type": "Point",
		                        "coordinates": [longitude, latitude]
		                    },
		                    "properties": {
		                        "Name": markerName,
		                        "Description": description,
		                        "Count" : numer_of_residents
		                    }
		                }]
		            }
		        },
		        'layout': {
		            'visibility': 'visible'
		        },
		        'paint': {
		            'circle-radius': 8,
		            'circle-color': 'red'
		        }
		    });

            // Create a popup, but don't add it to the map yet.
		    var popup = new mapboxgl.Popup({
		        closeButton: false,
		        closeOnClick: false
		    });

		    map.on('mouseenter', 'points'+marker_id, function(e) {
		        // Change the cursor style as a UI indicator.
		        map.getCanvas().style.cursor = 'pointer';

		        // Populate the popup and set its coordinates
		        // based on the feature found.
		        popup.setLngLat(e.features[0].geometry.coordinates)
		            .setHTML(e.features[0].properties.Name+"<br>"+e.features[0].properties.Description+"<br>Resisents Count : "+e.features[0].properties.Count)
		            .addTo(map);
		    });

		    map.on('mouseleave', 'points'+marker_id, function() {
		        map.getCanvas().style.cursor = '';
		        popup.remove();
		    });
}




function addMap(){
 // Fechting all the form values
 var name = $( "#idMapNameText" ).val();
 var upperRightLatitude = $( "#idUpperRightLat" ).val();
 var upperRightLongitude = $( "#idUpperRightLongitute" ).val();
 var lowerLeftLatitude = $( "#idLowerLeftLatitude" ).val();
 var lowerLeftLongitude = $( "#idLowerLeftLongitude" ).val();
 if(!name || !upperRightLatitude || !lowerLeftLatitude || !lowerLeftLongitude){
   $("#idMapFormMessage").removeClass("hidden");
   return;
  }

 addMapHelper(name,upperRightLatitude,upperRightLongitude,lowerLeftLatitude,lowerLeftLongitude);

}

function addMapHelper(name,upperRightLatitude,upperRightLongitude,lowerLeftLatitude,lowerLeftLongitude){
	$.ajax({
            url: '/pages/add_map',
            type: 'POST',
            dataType: 'json',
            data: {
                "map_name": name,
                "upper_right_lat": upperRightLatitude,
                "upper_right_long": upperRightLongitude,
                "lower_left_lat" : lowerLeftLatitude,
                "lower_left_long" : lowerLeftLongitude
            }
        })
        .done(function(response) {
               //alert(response.message);
               var map_id = response.map_id;
               var htmlToBeAdded = "<div class=\"row\" id=\"mapRow"+map_id+"\">\
                <div class=\"col-sm-6 pull-left\">"+name+"</div>\<div class=\"col-sm-2\">\<button type=\"button\" class=\"btn btn-success\" onClick=\"loadMap("+map_id+");\">View Map</button>\
                </div>\
                <div class=\"col-sm-2\">\
                	<button type=\"button\" class=\"btn btn-primary\" onClick=\"editMap("+map_id+");\">Edit</button>\
                </div>\
                <div class=\"col-sm-2 pull-right\">\
                	<button type=\"button\" class=\"btn btn-danger\" onClick=\"deleteMap("+map_id+");\">Delete</button>\
                </div>\
                </div>"; 
                $( "#idMapList" ).append(htmlToBeAdded);
                setFormFields('','','','','');
                $('#addMapModal').modal('toggle'); 
            })
        .fail(function(XMLHttpRequest, textStatus, errorThrown) {
            console.error("Error Occured!" + " | " + XMLHttpRequest.responseText +
                            " | " + textStatus + " | " + errorThrown);
        })
        .always(function() {});
}


function setFormFields(name,rightLat,rightLon,leftLat,leftLong){
 $( "#idMapNameText" ).val(name);
 $( "#idUpperRightLat" ).val(rightLat);
 $( "#idUpperRightLongitute" ).val(rightLon);
 $( "#idLowerLeftLatitude" ).val(leftLat);
 $( "#idLowerLeftLongitude" ).val(leftLong);
}

