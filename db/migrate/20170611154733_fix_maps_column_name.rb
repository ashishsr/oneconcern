class FixMapsColumnName < ActiveRecord::Migration
  	def change
       rename_column :maps, :lrlong, :llong
    end
end
