class CreateMarkers < ActiveRecord::Migration
  def change
    create_table :markers do |t|
      t.integer :map_id
      t.float :lat
      t.float :long
      t.string :name
      t.string :description
      t.integer :residents_count

      t.timestamps
    end
  end
end
