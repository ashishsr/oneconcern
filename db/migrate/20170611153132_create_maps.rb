class CreateMaps < ActiveRecord::Migration
  def change
    create_table :maps do |t|
      t.integer :user_id
      t.float :urlat
      t.float :urlong
      t.float :lllat
      t.float :lrlong

      t.timestamps
    end
  end
end
